// ros
#include <ros/ros.h>

// project
#include "robot_driver.h"

int main(int argc, char **argv)
{
// ros
    ros::init(argc, argv, "robot_driver");

    ros::Time::init();

    ros::NodeHandle nh;

    ros::Rate loop_rate(20);

// robot driver
    RobotDriver *robotDriver = new RobotDriver(nh);

    int ccounter = 0;

    while (ros::ok())
    {
        ros::spinOnce();

        loop_rate.sleep();

        robotDriver->trigger();

        if (ccounter++ >  5 )
        {
            ccounter = 0;

            robotDriver->update();
        }
    }

    return 0;
}
