#include "robot_driver.h"

RobotDriver::RobotDriver(ros::NodeHandle nh) :
        nh_(nh), rxCnt(0), txCnt(0),
        lock_motion(true), lock_counter(11),
        reset_motors(false), reset_stage(0)
{
    ros::NodeHandle nh_private("~");

// parameters
    nh_private.param<std::string> ("port_name", port_name, "/dev/sds_driver");
    nh_private.param<int> ("velocity_ramp_size", velocity_ramp_size, 10);
    nh_private.param<bool> ("velocity_inverse", velocity_inverse, false);
    nh_private.param<double> ("velocity_linear_max", velocity_linear_max, 0.25);
    nh_private.param<double> ("velocity_angular_max", velocity_angular_max, 0.5);
    nh_private.param<double> ("warning_distance", warning_distance, 0.4);
    nh_private.param<double> ("block_distance", block_distance, 0.2);

    nh_private.param<int> ("lights_cycle_loops", lights_cycle_loops, 6);

    nh_private.getParam("par_wheels_distance", par_wheels_distance);
    nh_private.getParam("par_wheel_radius", par_wheel_radius);
    nh_private.getParam("par_ipr", par_ipr);

// driver
    NFv2_Config2(&NFComBuf, NF_TerminalAddress, NF_RobotAddress);

    crcInit();

    CommPort = new SerialComm();
    CommPort->connect(port_name, 57600);

    if (!(CommPort->isOpen()))
    {
        std::cerr << "Connection failed to " << port_name << std::endl;

        ros::shutdown();
    }
    else
    {
        std::cout << "Connected to " << port_name << std::endl;
    }

    NFComBuf.SetDrivesSpeed.data[0] = 0;
    NFComBuf.SetDrivesSpeed.data[1] = 0;

// connections
    subCmdVel = nh_.subscribe("robot/cmd_vel", 1, &RobotDriver::callbackCmdVel, this);
    subState = nh_.subscribe("state", 1, &RobotDriver::callbackState, this);
    subLights = nh_.subscribe("robot/lights", 1, &RobotDriver::callbackLights, this);

    pubSensors = nh_.advertise<robot_msgs::sensors > ("sensors", 1);
    pubOdom = nh_.advertise<nav_msgs::Odometry > ("odom", 1);
    pubEncoders = nh_.advertise<robot_msgs::encoders > ("encoders", 1);
    pubVitals = nh_.advertise<robot_msgs::vitals > ("vitals", 1);

// robot
    robot = new Robot(velocity_ramp_size);

    robotStop();
}

RobotDriver::~RobotDriver()
{
    delete robot;

    robotPanic();
}

// -----------------------------------------------------------------------------
// update
// -----------------------------------------------------------------------------

void RobotDriver::trigger() // Synchronism of fastest communication
{
    driverGetVitals();

// check communication
    if (CommPort->error)
    {
        ROS_ERROR("ERROR IN CONNECTION");
        ros::shutdown();
    }

// lock
    if (lock_counter < 10)
    {
        lock_counter++;

        if (lock_motion && !reset_motors)
        {
            ROS_INFO("Unlocked! (cmd_vel message arrived)");

            lock_motion = false;
        }
    }
    else
    {
        if (!lock_motion)
        {
            ROS_INFO("Locked! (no cmd_vel message)");

            lock_motion = true;
        }

        robotStop();
    }

    if (!lock_motion)
    {
        if ( (!reset_motors) && ((robot->vitals.voltage_M1_V < 1.0) || (robot->vitals.voltage_M2_V < 1.0)) )
        {
            reset_motors = true;
            reset_stage = 0;
        }
        else
        {
            calculateRampSpeed();
            driverMotorsSpeeds(robot->velocity_linear_out, robot->velocity_angular_out);
        }
    }

    if (reset_motors)
    {
        driverMotorsReset();
    }

    dataToDriver();

    dataFromDriver();

    publishPosition();
}


void RobotDriver::update()
{
    driverGetDI();

    driverGetAI();

    driverSetDO();

//    driverSetAO();

    handlelights();

    dataToDriver();

    dataFromDriver();

    publishVitals();

    publishSensors();
}

 // robot panic functions
void RobotDriver::robotStop()
{
    robot->velocity_linear = 0.0;
    robot->velocity_angular = 0.0;

    driverMotorsSpeeds(0.0, 0.0);
}

void RobotDriver::robotPanic()
{
    robotStop();

    driverMotorsMode(0);
}

// -----------------------------------------------------------------------------
// callbacks
// -----------------------------------------------------------------------------

void RobotDriver::callbackCmdVel(const geometry_msgs::TwistConstPtr &msg)
{
    lock_counter = 0;

    robot->velocity_linear = msg->linear.x;
    robot->velocity_angular = msg->angular.z;

    handleLimits();

    if(velocity_inverse)
    {
      robot->velocity_linear = -robot->velocity_linear;
      robot->velocity_angular = -robot->velocity_angular;
    }

}


 void RobotDriver::callbackLights(const robot_msgs::lights &lights)
 {
     if (lights.lights_error_lock) {
         robot->lights.lights_error_lock = true;
         robot->lights.lights_auto_lock = false;
         robot->lights.lights_cruise_lock = false;
         robot->lights.lights_front_lock = false;
     }

     else if (lights.lights_auto_lock) {
         robot->lights.lights_auto_lock = true;
         robot->lights.lights_error_lock = false;
         robot->lights.lights_cruise_lock = false;
     }

     else if (lights.lights_cruise_lock) {
         robot->lights.lights_cruise_lock = true;
         robot->lights.lights_error_lock = false;
         robot->lights.lights_auto_lock = false;
         robot->lights.lights_front_lock = false;
     }

     else if (lights.lights_front_lock) {
         robot->lights.lights_front_lock = true;
         robot->lights.lights_error_lock = false;
         robot->lights.lights_cruise_lock = false;
     }

     else if (lights.lights_end_lock) {
         robot->lights.lights_end_lock = false;
         robot->lights.lights_error_lock = false;
         robot->lights.lights_auto_lock = false;
         robot->lights.lights_cruise_lock = false;
         robot->lights.lights_front_lock = false;
     }

 }

void RobotDriver::callbackState(const robot_msgs::state &msg)
{
    if (msg.force)
    {
        robot->DO[0] = msg.DO;
        robot->DO[1] = msg.D1;
        robot->DO[2] = msg.D2;
    }

    driverSetDO();
}

// -----------------------------------------------------------------------------
// publish - functions publishing stuff
// -----------------------------------------------------------------------------

void RobotDriver::publishPosition()
{
    driverGetEncoders();

    calculatePosition();

    publishEncoders();

    publishOdometry();

    publishTF();
}

void RobotDriver::publishEncoders()
{
    robot_msgs::encoders encoders;

    encoders.header.stamp = time_curr;

    encoders.left = robot->encoders[0];
    encoders.right = robot->encoders[1];

    pubEncoders.publish(encoders);
}

void RobotDriver::publishOdometry()
{
    nav_msgs::Odometry act_odom;

    act_odom.header.stamp = time_curr;
    act_odom.header.frame_id = "odom";
    act_odom.child_frame_id = "base_link"; //footprint

    act_odom.pose.pose.position.x = robot->position_x;
    act_odom.pose.pose.position.y = robot->position_y;
    act_odom.pose.pose.position.z = 0.0;
    act_odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw(robot->position_theta);

    act_odom.pose.covariance[Ecov_tx] = 1e-5; //pos_cov_px
    act_odom.pose.covariance[Ecov_ty] = 1e-5; //pos_cov_py
    act_odom.pose.covariance[Ecov_tz] = 1e1;  //pos_cov_pz
    act_odom.pose.covariance[Ecov_rx] = 1.0;  //pos_cov_ox
    act_odom.pose.covariance[Ecov_ry] = 1.0;  //pos_cov_oy

    act_odom.twist.twist.linear.x = robot->global_velocity_x;
    act_odom.twist.twist.linear.y = robot->global_velocity_y;
    act_odom.twist.twist.linear.z = 0.0;
    act_odom.twist.twist.angular.x = 0.0;
    act_odom.twist.twist.angular.y = 0.0;
    act_odom.twist.twist.angular.z = robot->global_velocity_z;

    act_odom.twist.covariance[Ecov_tx] = 0.00001; //vel_cov_px
    act_odom.twist.covariance[Ecov_ty] = 0.00001; //vel_cov_py
    act_odom.twist.covariance[Ecov_tz] = 10.0;    //vel_cov_pz
    act_odom.twist.covariance[Ecov_rx] = 1.0;     //vel_cov_ox
    act_odom.twist.covariance[Ecov_ry] = 1.0;     //vel_cov_oy
    act_odom.twist.covariance[Ecov_rz] = 0.00001; //vel_cov_oz

    pubOdom.publish(act_odom);
}

void RobotDriver::publishTF()
{
    geometry_msgs::TransformStamped act_trans;

    act_trans.header.stamp = time_curr;
    act_trans.header.frame_id = "odom";
    act_trans.child_frame_id = "base_link";

    act_trans.transform.translation.x = robot->position_x;
    act_trans.transform.translation.y = robot->position_y;
    act_trans.transform.translation.z = 0.0;
    act_trans.transform.rotation = tf::createQuaternionMsgFromYaw(robot->position_theta);

    tf_broadcaster_.sendTransform(act_trans);
}

void RobotDriver::publishVitals()
{
    robot->vitals.header.stamp = time_curr;
    robot->vitals.connection = CommPort->isOpen();

    pubVitals.publish(robot->vitals);
}

void RobotDriver::publishSensors()
{
    robot->sensors.header.stamp = time_curr;

    pubSensors.publish(robot->sensors);
}


// -----------------------------------------------------------------------------
// lights
// -----------------------------------------------------------------------------

void RobotDriver::handlelights()
{
     bool lights_front = false;
     bool lights_rear = false;
     bool lights_left = false;
     bool lights_right = false;
     bool lights_flicker = false;

     robot->lightcounter++;
     if (robot->lightcounter == lights_cycle_loops) {
         robot->lightcounter = 0;
     }

     if (robot->lightcounter<lights_cycle_loops/2) {
         lights_flicker = true;
     }
     else {
         lights_flicker = false;
     }


     if (robot->lights.lights_error_lock) {
         if (lights_flicker) {
             lights_front = true;
             lights_rear = true;
             lights_left = true;
             lights_right = true;
         }

     }
     double linear = robot->velocity_linear_out;
     double angular = robot->velocity_angular_out;

     if (velocity_inverse) {
         linear = -linear;
         angular= -angular;
     }
     if (linear<0) {
         angular=-angular;
     }


     if (robot->lights.lights_auto_lock) {

         if (linear > 0.02) {
             lights_front = true;
         }

         if (linear < -0.02) {
             lights_rear = true;
         }

         if (angular > 0.02) {
             if (lights_flicker) {
                lights_left = true;
         }
         }

         if (angular < - 0.02) {
             if (lights_flicker) {
                lights_right = true;
         }
         }
     }

     if (robot->lights.lights_cruise_lock) {
             lights_front = true;
             lights_rear = false;
             lights_left = true;
             lights_right = true;

     }

     if (robot->lights.lights_front_lock) {
         lights_front = true;

     }


     if (lights_front)
     {
         setBit(1, 0);
         setBit(1, 1);

     }
     else
     {
         unsetBit(1, 0);
         unsetBit(1, 1);
     }

     if (lights_left)
     {
         setBit(1, 2);
         setBit(1, 4);
     }
     else
     {
         unsetBit(1, 2);
         unsetBit(1, 4);
     }

     if (lights_right)
     {
         setBit(1, 3);
         setBit(1, 5);
     }
     else
     {
         unsetBit(1, 3);
         unsetBit(1, 5);
     }

     if (lights_rear)
     {
         setBit(1, 6);
     }
     else
     {
         unsetBit(1, 6);
     }

     driverSetDO();
 }

void RobotDriver::handleLimits() {
  if (robot->velocity_linear>0.0) {
    if (robot->sensors.fl < block_distance || robot->sensors.fr < block_distance) {
      robot->velocity_linear=0;
      for (auto &v:robot->velocity_linear_list) {
        v=0.0;
      }
    }
    else if (robot->sensors.fl < warning_distance || robot->sensors.fr < warning_distance) {
      robot->velocity_linear*=0.5;
    }


    if (robot->sensors.rf <block_distance) {
      robot->velocity_linear=0;
      for (auto &v:robot->velocity_linear_list) {
        v=0.0;
      }
      if (robot->velocity_angular<0) {
        robot->velocity_angular=0;
        for (auto &v:robot->velocity_angular_list) {
          v=0.0;
        }
      }
    }
    else if (robot->sensors.rf <warning_distance) {
      robot->velocity_linear*=0.5;
      if (robot->velocity_angular<0) {
        robot->velocity_angular*=0.5;
      }
    }

    if (robot->sensors.lf <block_distance) {
      robot->velocity_linear=0;
      for (auto &v:robot->velocity_linear_list) {
        v=0.0;
      }
      if (robot->velocity_angular>0) {
        robot->velocity_angular=0;
        for (auto &v:robot->velocity_angular_list) {
          v=0.0;
        }
      }
    }
    else if (robot->sensors.lf <warning_distance) {
      robot->velocity_linear*=0.5;
      if (robot->velocity_angular>0) {
        robot->velocity_angular*=0.5;
      }
    }

    if (robot->sensors.r<block_distance) {
      if(robot->velocity_angular<0) {
        robot->velocity_angular=0;
        for (auto &v:robot->velocity_angular_list) {
          v=0.0;
        }
      }
    }

    if (robot->sensors.l<block_distance) {
      if(robot->velocity_angular>0) {
        robot->velocity_angular=0;
        for (auto &v:robot->velocity_angular_list) {
          v=0.0;
        }
      }
    }
    }


    if (robot->velocity_linear<0.0) {
        if (robot->sensors.rb <block_distance) {
          robot->velocity_linear=0;
          for (auto &v:robot->velocity_linear_list) {
            v=0.0;
          }
           if (robot->velocity_angular<0) {
             robot->velocity_angular=0;
             for (auto &v:robot->velocity_angular_list) {
               v=0.0;
             }
           }
        }
        else if (robot->sensors.rb <warning_distance) {
            robot->velocity_linear*=0.5;
            if (robot->velocity_angular<0) {
                robot->velocity_angular*=0.5;
           }
        }

        if (robot->sensors.lb <block_distance) {
          robot->velocity_linear=0;
          for (auto &v:robot->velocity_linear_list) {
            v=0.0;
          }
           if (robot->velocity_angular>0) {
             robot->velocity_angular=0;
             for (auto &v:robot->velocity_angular_list) {
               v=0.0;
             }
           }
        }
        else if (robot->sensors.lb <warning_distance) {
            robot->velocity_linear*=0.5;
            if (robot->velocity_angular>0) {
                robot->velocity_angular*=0.5;
           }
        }

        if (robot->sensors.r<block_distance) {
            if(robot->velocity_angular<0) {
              robot->velocity_angular=0;
              for (auto &v:robot->velocity_angular_list) {
                v=0.0;
              }
            }
        }

        if (robot->sensors.l<block_distance) {
            if(robot->velocity_angular>0) {
              robot->velocity_angular=0;
              for (auto &v:robot->velocity_angular_list) {
                v=0.0;
              }
            }
        }
    }


}



// -----------------------------------------------------------------------------
// driver
// -----------------------------------------------------------------------------

void RobotDriver::driverGetVitals()
{
    robot->vitals.voltage_battery_V = NFComBuf.ReadDeviceVitals.data[0] / 1000.0;
    robot->vitals.voltage_24v_V = NFComBuf.ReadDeviceVitals.data[1] / 1000.0;
    robot->vitals.voltage_12v_V = NFComBuf.ReadDeviceVitals.data[2] / 1000.0;
    robot->vitals.voltage_5v_V = NFComBuf.ReadDeviceVitals.data[3] / 1000.0;
    robot->vitals.voltage_M1_V = NFComBuf.ReadDeviceVitals.data[4] / 1000.0;
    robot->vitals.voltage_M2_V = NFComBuf.ReadDeviceVitals.data[5] / 1000.0;

    commandArray[commandCnt++] = NF_COMMAND_ReadDeviceVitals;
}

void RobotDriver::driverGetEncoders()
{
    robot->encoders_previous[0] = robot->encoders[0];
    robot->encoders_previous[1] = robot->encoders[1];

    if(!velocity_inverse)
    {
      robot->encoders[0] = NFComBuf.ReadDrivesPosition.data[0];
      robot->encoders[1] = NFComBuf.ReadDrivesPosition.data[1];
    }
    else
    {
      robot->encoders[0] = -NFComBuf.ReadDrivesPosition.data[0];
      robot->encoders[1] = -NFComBuf.ReadDrivesPosition.data[1];
    }

    commandArray[commandCnt++] = NF_COMMAND_ReadDrivesPosition;
}

void RobotDriver::driverGetDI()
{
    robot->DI[0] = NFComBuf.ReadDigitalInputs.data[0]&0xFF;
    robot->DI[1] = NFComBuf.ReadDigitalInputs.data[1]&0xFF;
    robot->DI[2] = NFComBuf.ReadDigitalInputs.data[2]&0xFF;

    commandArray[commandCnt++] = NF_COMMAND_ReadDigitalInputs;
}

void RobotDriver::driverSetDO()
{
    commandArray[commandCnt++] = NF_COMMAND_SetDigitalOutputs;

    NFComBuf.SetDigitalOutputs.data[0] = robot->DO[0];
    NFComBuf.SetDigitalOutputs.data[1] = robot->DO[1];
    NFComBuf.SetDigitalOutputs.data[2] = robot->DO[2];
}

void RobotDriver::driverGetAI()
{
//    robot->sensors.f = convertSharp(NFComBuf.ReadAnalogInputs.data[]);
    robot->sensors.fl = convertSharp(NFComBuf.ReadAnalogInputs.data[1]);
    robot->sensors.fr = convertSharp(NFComBuf.ReadAnalogInputs.data[2]);

//    robot->sensors.b = convertSharp(NFComBuf.ReadAnalogInputs.data[]);
//    robot->sensors.bl = convertSharp(NFComBuf.ReadAnalogInputs.data[]);
//    robot->sensors.br = convertSharp(NFComBuf.ReadAnalogInputs.data[]);

    robot->sensors.l = convertSharp(NFComBuf.ReadAnalogInputs.data[4]);
    robot->sensors.lf = convertSharp(NFComBuf.ReadAnalogInputs.data[0]);
    robot->sensors.lb = convertSharp(NFComBuf.ReadAnalogInputs.data[7]);

    robot->sensors.r = convertSharp(NFComBuf.ReadAnalogInputs.data[5]);
    robot->sensors.rf = convertSharp(NFComBuf.ReadAnalogInputs.data[3]);
    robot->sensors.rb = convertSharp(NFComBuf.ReadAnalogInputs.data[6]);

    commandArray[commandCnt++] = NF_COMMAND_ReadAnalogInputs;
}

void RobotDriver::driverSetAO()
{
//    commandArray[commandCnt++] = NF_COMMAND_SetAnalog....;
}

void RobotDriver::driverMotorsSpeeds(double linear, double angular)
{
    if (std::abs(linear) > velocity_linear_max)
    {
        linear = std::copysign(velocity_linear_max, linear);
    }

    if (std::abs(angular) > velocity_angular_max)
    {
        angular = std::copysign(velocity_angular_max, angular);
    }

    double rotation; // linear speed of a wheel in m/s

    rotation = (angular * par_wheels_distance / 2.0);

    double left, right; // angular speed for a wheel in rad/s

    left = (linear - rotation) / par_wheel_radius;
    right = (linear + rotation) / par_wheel_radius;

    int left_impulses, right_impulses; // angular speed converted to impulses/s

    left_impulses = static_cast<int>((left * par_ipr)/(2 * M_PI));
    right_impulses = static_cast<int>((right * par_ipr)/(2 * M_PI));

    NFComBuf.SetDrivesSpeed.data[0] = left_impulses;
    NFComBuf.SetDrivesSpeed.data[1] = right_impulses;

    commandArray[commandCnt++] = NF_COMMAND_SetDrivesSpeed;
}

void RobotDriver::driverMotorsMode(int mode)
{
    switch (mode)
    {
        case 0x00:
        {
            NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_ERROR;
            NFComBuf.SetDrivesMode.data[1] = NF_DrivesMode_ERROR;
            commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;
            break;
        }

        case 0x02:
        {
            NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_SPEED;
            NFComBuf.SetDrivesMode.data[1] = NF_DrivesMode_SPEED;
            commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;
            break;
        }
        default:
        {
            ROS_ERROR("Wrong NF_DrivesMode...");
        }
    }
}

void RobotDriver::driverMotorsReset()
{
    switch (reset_stage)
    {
        case 0:
        {
            driverMotorsMode(0);

            lock_motion = true;
            reset_stage = 1;
            reset_counter = 0;

            break;
        }
        case 1:
        {
            if (reset_counter++ > 10)
            {
                reset_stage = 2;
                reset_counter = 0;
            }

            break;
        }
        case 2:
        {
            if ((robot->vitals.voltage_M1_V < 1.0) || (robot->vitals.voltage_M2_V < 1.0))
            {
                reset_stage = 3;
                driverMotorsMode(0x02);
            }

            break;
        }
        case 3:
        {
            if (reset_counter++ > 100)
            {
                reset_stage = 0;
                reset_counter = 0;
            }

            if ((robot->vitals.voltage_M1_V > 20.0) || (robot->vitals.voltage_M2_V > 20.0))
            {
                reset_stage = 4;
            }

            break;
        }
        case 4:
        {
            reset_motors = false;
            reset_stage = 0;

            break;
        }
    }
}

// -----------------------------------------------------------------------------
// data - send to driver & read from driver
// -----------------------------------------------------------------------------

void RobotDriver::dataFromDriver()
{
    int global_counter = 0;

    int n = 0;

    while ((n = CommPort->serialRead(&rxBuf[this->rxCnt], 1)) == 1)
    {
        global_counter++;

        if (n < 0)
        {
            perror("read problem");
            exit(EXIT_FAILURE);
        }

        uint8_t commArr[16], commCnt, bytesReceived;

        if ((bytesReceived = NF_Interpreter(&NFComBuf, rxBuf, &(this->rxCnt), commArr, &commCnt)) > 0)
        {
            break;
        }

        if (this->rxCnt == 255)
        {
            this->rxCnt = 0;
            break;
        }
    }
}

void RobotDriver::dataToDriver()
{
    // If communication with requested
    if (commandCnt > 0)
    {
        txCnt = NF_MakeCommandFrame(&NFComBuf, txBuf, (const uint8_t*) commandArray, commandCnt, NF_RobotAddress);
        // Clear communication request
        commandCnt = 0;

        if (!CommPort->serialWrite(txBuf, txCnt))
        {
            ROS_ERROR("Node is in error mode.. restarting...");
            ros::shutdown();
        }
    }
}

// -----------------------------------------------------------------------------
// utils - help functions
// -----------------------------------------------------------------------------

void RobotDriver::calculatePosition()
{
    gettimeofday(&odom_t_end, NULL);
    long mtime, seconds, useconds;
    seconds = odom_t_end.tv_sec - odom_t_start.tv_sec;
    useconds = odom_t_end.tv_usec - odom_t_start.tv_usec;
    mtime = ((seconds) * 1000 + useconds / 1000.0) + 0.5;
    if (mtime > 500) ROS_WARN("Odom update time was: %d ms!!", (int) mtime);
    gettimeofday(&odom_t_start, NULL);

    int encoders_diff[2];

    encoders_diff[0] = -robot->encoders_previous[0] + robot->encoders[0];
    encoders_diff[1] = -robot->encoders_previous[1] + robot->encoders[1];

    double fis[2];
    fis[0] = (encoders_diff[0] / (double) par_ipr) * 2 * M_PI;
    fis[1] = (encoders_diff[1] / (double) par_ipr) * 2 * M_PI;

    double dist[2];
    dist[0] = fis[0] * par_wheel_radius;
    dist[1] = fis[1] * par_wheel_radius;

    double positon_avr = (dist[0] + dist[1]) / double(2.0);
    double orient_adj = -((dist[0] - dist[1]) / double(par_wheels_distance));

    robot->position_theta += fmod(orient_adj,M_PI);

    robot->position_x += (positon_avr * cos(robot->position_theta));
    robot->position_y += (positon_avr * sin(robot->position_theta));

    robot->global_velocity_x = (positon_avr * cos(orient_adj)) / (double)(mtime / 1000.0);
    robot->global_velocity_y = (positon_avr * sin(orient_adj))/ (double)(mtime / 1000.0);
    robot->global_velocity_z = fmod(orient_adj,M_PI) / (double)(mtime / 1000.0);
}

void RobotDriver::calculateRampSpeed()
{
    if (velocity_ramp_size < 2)
    {
        robot->velocity_linear_out = robot->velocity_linear;
        robot->velocity_angular_out = robot->velocity_angular;
    }
    else
    {
        double sum_velocity;

    // linear
        robot->velocity_linear_list.erase(robot->velocity_linear_list.begin());
        robot->velocity_linear_list.push_back(robot->velocity_linear);

        sum_velocity = 0.0;
        for (auto v : robot->velocity_linear_list)
        {
            sum_velocity += v;
        }

        robot->velocity_linear_out = sum_velocity / velocity_ramp_size;

    // angular
        robot->velocity_angular_list.erase(robot->velocity_angular_list.begin());
        robot->velocity_angular_list.push_back(robot->velocity_angular);

        sum_velocity = 0.0;
        for (auto v : robot->velocity_angular_list)
        {
            sum_velocity += v;
        }

        robot->velocity_angular_out = sum_velocity / velocity_ramp_size;
    }
}

double RobotDriver::convertSharp(double ADC)
{
    double dist_m;

    int A = 24000;
    double m = 0.1;
    double n = 34;
    double B = 1;

    dist_m = ((A / (m * ADC + n)) + B) / 1000.0;

    return dist_m;
}

void RobotDriver::setBit(int byte_number, int bit_number)
{
    uint8_t mask = 0x00;
    mask = (1 << bit_number);
    robot->DO[byte_number] |= mask;
}

void RobotDriver::unsetBit(int byte_number, int bit_number)
{
    uint8_t mask = 0x00;
    mask = ~(1 << bit_number);
    robot->DO[byte_number] &= mask;
}
