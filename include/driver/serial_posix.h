
#ifndef SERIALCOMM_H
#define	SERIALCOMM_H

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>

#include <iostream>

#include <sys/time.h>
#include "poll.h"

#ifdef MULTI_THREAD_RX

#include<queue>
#include<vector>

#endif

///default complete port name
#define DEFAULT_PORT "/dev/ttyACM0"
///default baudrate
#define DEFAULT_BAUD 57600
///serial sth delay [ms]
#define SERIAL_TIMEOUT_MS 70

#define RX_BUFF_MAX_LEN 255
#define TX_BUFF_MAX_LEN 255

class SerialComm
{
public:

    SerialComm(std::string _port_name = DEFAULT_PORT, int _baud_rate = DEFAULT_BAUD);

    ///destructor
    virtual ~SerialComm();

    ///connect
    bool connect(std::string _port_name = DEFAULT_PORT, int _baud_rate = DEFAULT_BAUD);

    bool disconnect();

    ///true when port is open
    bool isOpen();
    bool connected_;
    bool error;

    ///write data to serial
    bool serialWrite(uint8_t* buff, uint32_t len);
    ///read data from serial
    int serialRead(uint8_t* buff, uint32_t len);

private:

#ifdef MULTI_THREAD_RX
    typedef void* void_ptr_t;
    pthread_t recv_thread_;
    static void_ptr_t recvCallback(void * arg);
#endif

    void SERIAL_EXEPTION(std::string exp);
    void SERIAL_REOPEN(std::string exp);
    void SERIAL_ABORT(std::string exp);

    //port identifier
    int fd_;

    ///serial port name
    std::string port_name_;
    ///baudrate of transmission
    int baud_rate_;
    ///number of bytes to read
    int num_bytes_read_;
    ///number of bytes to write
    int num_bytes_write_;
    ///serial port read buffer
    uint8_t* read_buff_;
    ///serial port write buffer
    uint8_t* write_buff_;
    ///store serial port settings
    struct termios oldtio_;

#ifdef MULTI_THREAD_RX
    std::queue< std::vector<uint8_t> > rx_vec_;
#endif

    ///open port
    void openPort();
    ///close port
    void closePort();
    ///reopen port
    void reopenPort();

    ///communication timeout errors counter
    int ERR_timeout;
    ///number of accepted communication timeout errors
    static const int MAX_ERR_timeout = 10;
    ///read errors counter
    int ERR_readfailed;
    ///number of accepted read errors
    static const int MAX_ERR_readfailed = 10;
    ///read repeats counter
    int ERR_tryread;
    ///number of accepted read repeats
    static const int MAX_ERR_tryread = 2;
    ///sent configuration attempts counter
    int ERR_confsend;
    ///number of accepted sent configuration attempts
    static const int MAX_ERR_confsend = 10;
    ///reopen attempts counter
    int ERR_reopen;
    ///number of accepted reopen attempts
    static const int MAX_ERR_reopen = 10;
    ///crc errors counter
    int ERR_crc;
    ///number of accepted crc errors
    static const int MAX_ERR_crc = 10;
};

#endif	/* SERIALCOMM_H */
