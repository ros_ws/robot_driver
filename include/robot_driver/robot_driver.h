#ifndef ROBOT_DRIVER_H
#define	ROBOT_DRIVER_H

#define DEFAULT_NFV2_BAUD B57600
#define COMM_BUFSZ 256
#define COMMAND_ARRAY_SZ 16

// ROS
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

// ROS msgs
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>

// robot msgs
#include "robot_msgs/encoders.h"
#include "robot_msgs/lights.h"
#include "robot_msgs/state.h"
#include "robot_msgs/vitals.h"
#include "robot_msgs/sensors.h"

// driver
#include "mycrc.h"
#include "nfv2.h"
#include "serial_posix.h"

class Robot
{
public:

    Robot(int list_size) :
            velocity_linear(0.0), velocity_linear_out(0.0),
            velocity_angular(0.0), velocity_angular_out(0.0),
            position_x(0.0), position_y(0.0), position_theta(0.0),
            global_velocity_x(0.0), global_velocity_y(0.0), global_velocity_z(0.0), lightcounter(0)
    {
        if (list_size > 1)
        {
            velocity_linear_list.clear();
            velocity_angular_list.clear();

            for (int i = 0; i < list_size; ++i)
            {
                velocity_linear_list.push_back(0.0);
                velocity_angular_list.push_back(0.0);
            }
        }
        
        for(auto e : encoders)
            e = 0;
        
        for(auto e_p : encoders_previous)
            e_p = 0;
        
        for(auto d_i : DI)
            d_i = 0x00;
        
        for(auto d_o : DO)
            d_o = 0x00;
    }

    ~Robot() { }

// velocity
    double velocity_linear; // [m/s]
    double velocity_linear_out; // [m/s]
    std::vector<double> velocity_linear_list;

    double velocity_angular; // [rad/s]
    double velocity_angular_out; // [rad/s]
    std::vector<double> velocity_angular_list;

// position
    double position_x; // [m]
    double position_y; // [m]
    double position_theta; // [rad]

    double global_velocity_x;
    double global_velocity_y;
    double global_velocity_z;

// inputs & outputs
    uint8_t DO[3]; // digital_outputs
    uint8_t DI[3]; // digital_inputs

// encoders
    int encoders[2];
    int encoders_previous[2];

// vitals
    robot_msgs::vitals vitals;

// sensors
    robot_msgs::sensors sensors;
    robot_msgs::lights lights;
    int lightcounter;
    
};

class RobotDriver
{
public:
    RobotDriver(ros::NodeHandle nh);
    ~RobotDriver();

    void robotInit();
    void robotStop();
    void robotPanic();

    void update();
    void trigger();

private:
// callbacks
    void callbackCmdVel(const geometry_msgs::TwistConstPtr &msg);
    void callbackState(const robot_msgs::state &msg);
    void callbackLights(const robot_msgs::lights &lights);

// publish
    void publishPosition();
    void publishEncoders();
    void publishOdometry();
    void publishTF();

    void publishSensors();
    void publishVitals();
    
    
 // lights
    void handlelights();
    
 //ograniczenia sharpow
    void handleLimits();
  
// driver
    void driverGetVitals();
    void driverGetEncoders();

    void driverGetDI();
    void driverSetDO();

    void driverGetAI();
    void driverSetAO();

    void driverMotorsSpeeds(double linear, double angular);
    void driverMotorsMode(int mode);
    void driverMotorsReset();

// data
    void dataToDriver();
    void dataFromDriver();

// utils
    void calculateRampSpeed();
    void calculatePosition();
    double convertSharp(double ADC);
    
    void setBit(int byte_number, int bit_number);
    void unsetBit(int byte_number, int bit_number);

    Robot *robot;

// ROS
    ros::NodeHandle nh_;
    tf::TransformListener tf_listener_;
    tf::TransformBroadcaster tf_broadcaster_;

    ros::Subscriber subCmdVel, subState, subLights;
    ros::Publisher pubSensors, pubOdom, pubEncoders, pubVitals;

    ros::Time time_curr;

// msgs
    robot_msgs::state state;

// connection
    SerialComm *CommPort;

// parameters
    std::string port_name;

    double velocity_linear_max;
    double velocity_angular_max;
    int velocity_ramp_size;
    bool velocity_inverse;
    
    int lights_cycle_loops;

    double par_wheels_distance;
    double par_wheel_radius;
    int par_ipr;
    
    double warning_distance;
    double block_distance;

// driver
    NF_STRUCT_ComBuf NFComBuf;

    uint8_t commandArray[COMMAND_ARRAY_SZ];
    uint8_t rxCnt, txCnt, commandCnt;

    uint8_t rxBuf[COMM_BUFSZ];
    uint8_t txBuf[COMM_BUFSZ];

// lock
    bool lock_motion;
    int lock_counter;

// reseting motors
    bool reset_motors;
    int reset_stage;
    int reset_counter;

// odom utils
    struct timeval odom_t_start, odom_t_end;

    enum ECOV {
        Ecov_tx = 0,
        Ecov_ty = 7,
        Ecov_tz = 14,
        Ecov_rx = 21,
        Ecov_ry = 28,
        Ecov_rz = 35
    };

};

#endif	/* ROBOT_DRIVER_H */
